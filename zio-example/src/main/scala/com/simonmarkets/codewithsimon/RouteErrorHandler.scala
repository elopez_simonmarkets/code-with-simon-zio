package com.simonmarkets.codewithsimon

import akka.http.interop.ErrorResponse
import com.simonmarkets.codewithsimon.MyServiceErrors._
import com.simonmarkets.http.HttpError
import com.simonmarkets.logging.{TraceId, TraceLogging}

trait RouteErrorHandler extends TraceLogging {

  implicit def coreErrorResponse(implicit traceId: TraceId): ErrorResponse[MyZIOError] = {
    case BadRequest(message) =>
      HttpError.badRequest(message).httpResponse
    case NotFound(message) =>
      HttpError.notFound(message).httpResponse
    case NotAuthorized(message) =>
      HttpError.unauthorized(message).httpResponse
    case err: Internal =>
      log.error(s"InternalServerError: caused by ${err.reason}")
      HttpError.internalServerError("Request failed due to Internal Server Error.").httpResponse
  }

}