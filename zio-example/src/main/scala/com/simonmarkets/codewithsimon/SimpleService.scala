package com.simonmarkets.codewithsimon

import com.goldmansachs.marquee.pipg.UserACL
import com.simonmarkets.logging.{TraceId, TraceLogging}
import com.simon.annuities.indexannuity.domain.ppfa.PPFATemplate
import com.simon.annuities.indexannuity.domain.ppfa.loader.PPFAValidatingLoader
import com.simonmarkets.annuities.core.repository.IndexAnnuityPolicyRepository.Filter
import com.simonmarkets.annuities.core.repository.{IndexAnnuityPolicyRepository, PPFATemplateRepository}
import com.simonmarkets.annuities.indexannuity.domain.IndexAnnuityRates
import com.simonmarkets.om.contract.{DocumentDescriptor, OfferingViewWithContext}
import com.simonmarkets.om.contract.domain.AssetClass
import com.simonmarkets.utils.pagination.model.{Page, Paging}

import java.io.ByteArrayInputStream

import scala.concurrent.{ExecutionContext, Future}

class SimpleService(
    ppfaTemplateRepository: PPFATemplateRepository,
    policyRepository: IndexAnnuityPolicyRepository
) extends TraceLogging with CommonFuncs {

  def savePPFA(
    message: PPFAPostPayload,
    cusip: String,
    defaultProductCode: String
  )(implicit
    ex: ExecutionContext,
    traceId: TraceId,
    user: UserACL
  ): Future[Seq[IndexAnnuityServicePPFAUploadResponse]] = {
    log.info("Attempting to save PPFA")

    val xmlContent        = new ByteArrayInputStream(message.contents)
    val loader            = new PPFAValidatingLoader(xmlContent)
    val ppfaEffectiveDate = message.effectiveDate
    for {
      ppfas <- extract(loader, cusip, defaultProductCode, message.productCodes, ppfaEffectiveDate, message.assetClass)
        .fold(left => Future.failed(new Exception(left.mkString(","))), right => Future.successful(right))

      _ = log.info(s"PPFA validated. Number of annuities extracted = ${ppfas.size}")

      issuerKey = ppfas.head.params.issuerKey
      entitlements <- checkEntitlements(message.assetClass.name, issuerKey, user.userId, "Edit")
      _ <-
        if (entitlements.authorized)
          Future.unit
        else
          Future.failed(
            new Exception(
              s"${user.userId} is not authorized to upload ${message.assetClass} PPfA for issuer ${issuerKey}"
            )
          )
      // Send to document service asynchronously
      _ = log.info(s"Sending PPFA file to document service")
      _ = ppfas.map(x => saveDocument(x.params.cusip, message.contents, None, "PPFA", user.userId))
      _ = log.info(s"Saving PPFA templates")
      _ <- Future.sequence(ppfas.map(upsertPPFA(user.userId, _))) // add retry example
      _ = log.info(s"All PPFA tasks completed successfully")
    } yield ppfas.map(IndexAnnuityServicePPFAUploadResponse)
  }

  def getAnnuityIssuances(message: CarrierPortalIssuancesRequest)
    (implicit ex: ExecutionContext, traceId: TraceId, user: UserACL): Future[Page[CarrierPortalIssuancesResponse]] = {
    for {
      access         <- getPermissions(user)
      assetClasses   =  message.contractTypeNames.map(_.map(AssetClass(_)))
      ppfas          <- ppfaTemplateRepository.findLatest(message.effectiveDateISO, assetClasses, access.issuerKeys, message.paging)
      ppfasCount     <- ppfaTemplateRepository.countLatest(message.effectiveDateISO, assetClasses, access.issuerKeys)
      cusips         =  ppfas.map(_.params.cusip).toSet
      policies       <- policyRepository.getAllAnnuityPoliciesWindow(
        filter = Filter(identifiers = Some(cusips)),
        effectiveDateISO = message.effectiveDateISO,
        pastPoliciesPerCusip = 5
      )
      policyMap            =  policies.groupBy(_.params.cusip)
      contractOfferings    <- getContractOfferings(cusips, user.userId)
      contractOfferingsMap =  contractOfferings.groupBy(_.id)
      documents         <- loadDocumentInfo(cusips, message.effectiveDateISO)
    } yield {
      val items = ppfas.map { ppfa =>
        val offering = contractOfferingsMap.getOrElse(ppfa.params.cusip, List.empty)

        CarrierPortalIssuancesResponse(
          ppfa.params.cusip, ppfa,
          policyMap.get(ppfa.params.cusip).map { policies =>
            policies.map(IndexAnnuityRates.apply)
          },
          documents,
          offering
        )
      }
      Page(
        count = ppfasCount,
        offset = message.paging.offset,
        limit = message.paging.limit,
        items = items)
    }
  }

}

case class PPFAPostPayload(
  assetClass: AssetClass,
  productCodes: Option[Set[String]],
  effectiveDate: String,
  contents: Array[Byte],
  cusip: Option[String] = None
)

case class Permissions(issuerKeys: Option[Set[String]])

case class IndexAnnuityServicePPFAUploadResponse(ppfa: PPFATemplate)

case class Entitled(authorized: Boolean)

case class DocUploadResult(succeeded: Boolean)

case class PPFAUpsertResult(succeeded: Boolean)

case class CarrierPortalIssuancesResponse(
    cusip: String,
    ppfaTemplate: PPFATemplate,
    policyDetails: Option[List[IndexAnnuityRates]],
    documents: List[DocumentDescriptor],
    offering: List[OfferingViewWithContext]
)

case class IssuanceDocumentResponse(status: Int, documents: Option[List[DocumentDescriptor]])

case class CarrierPortalIssuancesRequest(
    effectiveDateISO: String,
    contractTypeNames: Option[Set[String]],
    paging: Paging)