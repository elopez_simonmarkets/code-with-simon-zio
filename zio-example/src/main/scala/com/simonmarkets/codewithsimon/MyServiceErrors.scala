package com.simonmarkets.codewithsimon

object MyServiceErrors {

  sealed trait MyZIOError {

    def reason: String

  }

  final case class BadRequest(reason: String) extends MyZIOError

  final case class Internal(reason: String, cause: Option[Throwable]) extends MyZIOError

  final case class NotAuthorized(reason: String) extends MyZIOError

  final case class NotFound(reason: String) extends MyZIOError

  object Internal {

    def apply(
        message: String,
        error: Throwable
    ): Internal = Internal(s"$message due to [${error.getMessage}]", Option(error.getCause))

  }

}
