package com.simonmarkets.codewithsimon

import com.goldmansachs.marquee.pipg.UserACL
import com.simon.annuities.indexannuity.domain.ppfa.PPFATemplate
import com.simon.annuities.indexannuity.domain.ppfa.loader.PPFAValidatingLoader
import com.simonmarkets.om.contract.{DocumentDescriptor, OfferingViewWithContext}
import com.simonmarkets.om.contract.domain.AssetClass

import scala.concurrent.{ExecutionContext, Future}

trait CommonFuncs {

  def getContractOfferings(cusips: Set[String], userId: String): Future[List[OfferingViewWithContext]] = {
    Future.failed(new Exception)
  }

  def loadDocumentInfo(cusips: Set[String], effectiveDateFrom: String): Future[List[DocumentDescriptor]] = {
    Future.failed(new Exception)
  }

  def getPermissions(user: UserACL): Future[Permissions] = {
    Future.failed(new Exception)
  }

  def checkEntitlements(contractType: String, issuerKey: String, userToCheck: String, action: String) : Future[Entitled] = {
    Future.successful(Entitled(true))
  }

  def saveDocument(
      cusip: String, contents: Array[Byte], metadata: Option[String], documentId: String,
      userId: String)(implicit ex: ExecutionContext) = {
    Future.successful(DocUploadResult(true))
  }

  def upsertPPFA(userId: String, ppfa: PPFATemplate): Future[PPFAUpsertResult] = {
    Future.successful(PPFAUpsertResult(true))
  }

  def extract(
    loader: PPFAValidatingLoader,
    cusip: String,
    defaultProductCode: String,
    productCodes: Option[Set[String]],
    ppfaEffectiveDate: String,
    assetClass: AssetClass
  ): Either[Seq[String], Seq[PPFATemplate]] = {
    loader.validateAndExtractPPFAsByCusip(cusip, Some(defaultProductCode), productCodes, ppfaEffectiveDate, assetClass)
  }

}
