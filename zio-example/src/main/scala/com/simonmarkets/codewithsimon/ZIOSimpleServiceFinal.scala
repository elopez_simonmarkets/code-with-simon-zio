package com.simonmarkets.codewithsimon

import com.goldmansachs.marquee.pipg.UserACL
import com.simon.annuities.indexannuity.domain.ppfa.PPFATemplate
import com.simon.annuities.indexannuity.domain.ppfa.loader.PPFAValidatingLoader
import com.simonmarkets.annuities.core.repository.IndexAnnuityPolicyRepository.Filter
import com.simonmarkets.annuities.core.repository.{IndexAnnuityPolicyRepository, PPFATemplateRepository}
import com.simonmarkets.annuities.indexannuity.domain.IndexAnnuityRates
import com.simonmarkets.codewithsimon.MyServiceErrors._
import com.simonmarkets.logging.{TraceId, TraceLogging}
import com.simonmarkets.om.contract.domain.AssetClass
import com.simonmarkets.utils.pagination.model.Page
import zio.{IO, UIO, ZIO}

import java.io.ByteArrayInputStream

class ZIOSimpleServiceFinal(
    ppfaTemplateRepository: PPFATemplateRepository,
    policyRepository: IndexAnnuityPolicyRepository
) extends TraceLogging
    with CommonFuncs {

  def getAnnuityIssuances(message: CarrierPortalIssuancesRequest)
    (implicit traceId: TraceId, user: UserACL): IO[MyZIOError, Page[CarrierPortalIssuancesResponse]]= {
    for {
      access         <- ZIO.fromFuture(_ => getPermissions(user))
        .mapError(Internal("Issue fetching permissions", _))
      assetClasses   <- ZIO.effect(message.contractTypeNames.map(_.map(AssetClass(_))))
        .mapError(Internal("Issue mapping asset classes", _))
      ppfasF          <- ZIO.fromFuture(_ => ppfaTemplateRepository
        .findLatest(message.effectiveDateISO, assetClasses, access.issuerKeys, message.paging))
        .mapError(Internal("Issue fetching ppfas", _))
        .fork
      ppfasCountF     <- ZIO.fromFuture(_ => ppfaTemplateRepository
        .countLatest(message.effectiveDateISO, assetClasses, access.issuerKeys))
        .mapError(Internal("Issue counting ppfas", _))
        .fork

      ppfas      <- ppfasF.join
      ppfasCount <- ppfasCountF.join

      cusips         =  ppfas.map(_.params.cusip).toSet

      policiesF       <- ZIO.fromFuture(_ => policyRepository
        .getAllAnnuityPoliciesWindow(
          filter = Filter(identifiers = Some(cusips)),
          effectiveDateISO = message.effectiveDateISO,
          pastPoliciesPerCusip = 5))
        .mapError(Internal("Issue fetching policies", _))
        .fork
      contractOfferingsF <- ZIO.fromFuture(_ => getContractOfferings(cusips, user.userId))
        .mapError(Internal("Issue fetching offerings", _))
        .fork
      documentsF         <- ZIO.fromFuture(_ => loadDocumentInfo(cusips, message.effectiveDateISO))
        .mapError(Internal("Issue fetching documents", _))
        .fork

      policies          <- policiesF.join
      contractOfferings <- contractOfferingsF.join
      documents         <- documentsF.join
    } yield {
      val policyMap            =  policies.groupBy(_.params.cusip)
      val contractOfferingsMap =  contractOfferings.groupBy(_.id)

      val items = ppfas.map { ppfa =>
        val offering = contractOfferingsMap.getOrElse(ppfa.params.cusip, List.empty)

        CarrierPortalIssuancesResponse(
          ppfa.params.cusip, ppfa,
          policyMap.get(ppfa.params.cusip).map { policies =>
            policies.map(IndexAnnuityRates.apply)
          },
          documents,
          offering
        )
      }

      Page(
        count = ppfasCount,
        offset = message.paging.offset,
        limit = message.paging.limit,
        items = items)
    }
  }

  def savePPFA(message: PPFAPostPayload, cusip: String, defaultProductCode: String)
    (implicit traceId: TraceId, user: UserACL): IO[Any, Seq[IndexAnnuityServicePPFAUploadResponse]] = {
    for {
      _ <- UIO(log.info("Attempting to save PPFA"))

      xmlContent <- ZIO
        .effect(new ByteArrayInputStream(message.contents))
        .mapError(Internal("Issue loading payload contents", _))
      loader <- ZIO.effect(new PPFAValidatingLoader(xmlContent)).mapError(Internal("Issue loading xml contents", _))
      ppfaEffectiveDate = message.effectiveDate
      ppfas <- ZIO
        .fromEither {
          extract(loader, cusip, defaultProductCode, message.productCodes, ppfaEffectiveDate, message.assetClass)
        }
        .mapError(messages => BadRequest(messages.mkString(", ")))

      _ = log.info(s"PPFA validated. Number of annuities extracted = ${ppfas.size}")
      entitlements <- ZIO
        .fromFuture {
          _ => checkEntitlements(message.assetClass.name, ppfas.head.params.issuerKey, user.userId, "Edit")
        }
        .mapError(Internal("Issue checking user entitlements", _))
      _ <- ZIO.fail(NotAuthorized("User is not authorized")).unless(entitlements.authorized)

      _ <- ZIO.foreachParN_(n = 5)(ppfas)(savePPFATemplate)
    } yield ppfas.map(IndexAnnuityServicePPFAUploadResponse)
  }

  def savePPFATemplate(ppfa: PPFATemplate)(implicit traceId: TraceId, user: UserACL): IO[Internal, PPFAUpsertResult] =
    ZIO.fromFuture(_ => upsertPPFA(user.userId, ppfa)).mapError(Internal("", _))

}
